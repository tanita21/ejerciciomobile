package cleanTest;

import activity.whenDo.MainScreen;
import activity.whenDo.MyListForm;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import singletonSession.Session;

public class CreUpDelNoteWhenDoTest {
    MainScreen mainScreen = new MainScreen();
    MyListForm createNoteForm = new MyListForm();
    MyListForm updateNoteForm = new MyListForm();

    @Test
    public void verifyCreUpDelNewNote(){
        String title="Nota Ejercicio 1";
        String note="Esta es una nota para ver la creacion";

        mainScreen.addNoteButton.click();
        createNoteForm.titleTxtBox.setText(title);
        createNoteForm.noteTxtBox.setText(note);
        createNoteForm.saveButton.click();

        Assertions.assertTrue(mainScreen.isNoteDisplayed(title),
                "ERROR, the note was not created");

        String upTitle="Nota Modificada Ejercicio 1";
        String upNote="Esta es una nota modificada";

        mainScreen.selectNote(title);
        updateNoteForm.titleTxtBox.setText(upTitle);
        updateNoteForm.noteTxtBox.setText(upNote);
        updateNoteForm.saveButton.click();

        Assertions.assertTrue(mainScreen.isNoteDisplayed(upTitle),
                "ERROR, the note was not updated");

        mainScreen.selectNote(upTitle);
        updateNoteForm.deleteButton.click();
        updateNoteForm.deleteConfirmButton.click();
        Assertions.assertFalse(mainScreen.isNoteDisplayed(upTitle),
                "ERROR, the note was not deleted");
    }

    @AfterEach
    public void closeApp(){
        Session.getInstance().closeApp();
    }



}
